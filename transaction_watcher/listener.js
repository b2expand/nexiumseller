const Web3 = require("web3");
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const web3 = new Web3(new Web3.providers.WebsocketProvider("ws://192.168.0.252:8546"));

const networks = {
    "0": "error",
    "1": "main network",
    "2": "morden",
    "3": "ropsten",
    "4": "rinkeby",
    "42": "kovan"
};

let txToWatch = [];

function log(string) {
    console.log((new Date()).toISOString() + " " + string);
}

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.post("/", function (req, res) {
    if (!req.body.rawTx || !req.body.publicAddress || !req.body.value) {
        res.status(400).send("One or severals fields are missing");
        log("A registration was refused from " + req.ip + " : invalid request");

    } else if (!web3.utils.isAddress(req.body.publicAddress)) {
        res.status(400).send("The user address seems wrong");
        log("A registration was refused from " + req.ip + " : invalid address");

    } else if (!web3.utils.isHex(req.body.rawTx)) {
        res.status(400).send("The raw transaction is not an hex string");
        log("A registration was refused from " + req.ip + " : rawTx not hexadecimal");

    } else {
        try {
            let val = web3.utils.toBN(req.body.value);
            txToWatch.push({
                rawtx: req.body.rawTx,
                publicAddress: req.body.publicAddress,
                value: val
            });
            res.send("New transaction registered");
            log("New transaction registered from " + req.body.publicAddress);
        } catch (e) {
            res.status(400).send("The value is not a valid number");
            log("A registration was refused from " + req.ip + " : invalid value");
        }

    }
});

web3.eth.net.getId().then((result) => {
    if (networks.hasOwnProperty(result)){
        console.log("Connected to " + networks[result]);
    } else {
        console.log("Connected to custom network");
    }
});


web3.eth.subscribe("newBlockHeaders", (error, block) => {
    web3.eth.getBlock(block.number).then((block) => {
        log("New block " + "#" + block.number);
        if (txToWatch.length > 0 && block.transactions.length > 0) {
            for (let i in txToWatch) {
                let tx = txToWatch[i];

                function onBalance(res) {
                    if (web3.utils.toBN(res).cmp(tx.value) >= 0) {
                        web3.eth.sendRawTransaction(tx.rawTx);
                        log("Tx of " + tx.publicAddress + " sent");
                        txToWatch.splice(i, 1);
                    } else {
                        log("Nothing new for " + tx.publicAddress);
                    }
                }

                web3.eth.getBalance(tx.publicAddress)
                    .then(onBalance);
            }
        } else {
            log("Nothing to check on the new block");
        }
    });
});

app.listen(14000);
