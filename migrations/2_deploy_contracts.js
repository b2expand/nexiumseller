let NexiumSeller = artifacts.require("./NexiumSeller.sol");
let Nexium = artifacts.require("./Nexium.sol");

//b0f6cc9e571a77d3879a77e2489b40482e7fef1a2326a665879fe98a94853761
let oracle = "0x2F761425983207D08D777B2B25d6967A91d32Ac1";

module.exports = function (deployer) {
    deployer.deploy(Nexium).then((instance)=>{
        return deployer.deploy(NexiumSeller, instance.address, oracle)
    });
};
