pragma solidity ^0.4.15;


import "zeppelin-solidity/contracts/ownership/Ownable.sol";
import "zeppelin-solidity/contracts/token/ERC20.sol";
import "zeppelin-solidity/contracts/lifecycle/Pausable.sol";




contract NexiumSeller is Ownable, Pausable{
    address oracle;

    ERC20 nexiums;

    event OracleChanged(address newOracle);

    function NexiumSeller(address orcl, address nexiumContract) {
        owner = msg.sender;
        oracle = orcl;
        nexiums = ERC20(nexiumContract);
    }

    function changeOracle(address newOracle) {
        require(msg.sender == oracle);
        oracle = newOracle;
        OracleChanged(newOracle);
    }

    function getNexiums(uint256 rate, uint64 timestamp, bytes32 h, uint8 v, bytes32 r, bytes32 s) payable whenNotPaused() {
        require(now <= timestamp);
        bytes32 hedData = sha3(rate, timestamp);
        require(hedData == h);
        address signer = ecrecover(h, v, r, s);
        require(signer == oracle);
        uint256 nxcSold = (msg.value / rate) * 1000 + 1;
        require(nexiums.transfer(msg.sender, nxcSold));
        owner.send(msg.value);
    }
}
